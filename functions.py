from io import *
import os
import sys

class Character:
    def __init__(self, name, level, max_health):
        self.name = name
        self.level = level
        self.max_health = max_health
        self.backpack = []
    
    def open_chest(self, chest):
        return f'You got the: {chest}'

    def show_backpack(self):
        for item in self.backpack:
            print(f'{item.name} - {item.description}')
       
        
class Monsters(Character):
    def __init__(self, name, level, max_health, abilities):
        super().__init__(name, level, max_health)
        self.abilities = abilities

class Items:
    def __init__(self, name, description):
        self.name = name
        self.description = description
        
    def add_items(self, character):
        character.backpack.append(self)

class Rooms(Items):
    def __init__(self, name, description):
        super().__init__(name, description)

    def entry(self, room):
        return f"You just entered the {room}"

    def exit(self,room):
        return f"You just got out of {room}"

class Mystery: 
    def __init__(self, name): 
        self.name = name
# Sprawdzenie odpowiedzi zagadki
    def first_riddle(self, player):
        correct_answear = 2
        while True:
            player = int(input("Aswear: "))
            if player == correct_answear:
                print("Good job!")
                break
            else:
                print("Don't make that noise or you'll wake the dragon")

class Menu:
    def __init__(self, title):
        self.title = title
        
# Start GAME MENU
    def show_options(self, character):
        character = int(input("1. Start game\n2. Exit\n"))
        os.system('clear')
        if character == 2:
            sys.exit()
        if character == 1:
            character = int(input("1. Show instructions\n2. Don't show instructions\n"))
            if character == 1:
                os.system('clear')
                print(game_data.instructions)
                print("Lets start the game!")
            else:
                os.system('clear')
                pass
            
    def show_instructions(self):
        print()

    def interface(self):
        print("-------          ----------")

class GameData:
    def __init__(self,path:str):
        self.path = path
        self.instructions = None
        self.first_room = None
        self.second_room = None
        self.corridor = None
        self.go_home = None
        self.mystery = None
        self.end_game = None
        self.item = None
        

    def read_data(self):
        with open(self.path, "r") as file:
            self.instructions = file.readline()
            self.first_room = file.readline()
            self.second_room = file.readline()
            self.corridor = file.readline()
            self.go_home = file.readline()
            self.mystery = file.readline()
            self.end_game = file.readline()

            

class Game(Menu):
    def __init__(self, player_name, level):
        self.player_name = player_name
        self.level = level
        self.chest_opened = False

    def start_game(self):
        print()
        print(f"Welcome in game {menu.title}")
        self.show_options(bilbo)

    def play(self):
        entrance.entry("Hobbit castle")
        print(game_data.first_room)
        player = int(input("1. Left\n2. Right\nYour choice?: "))
        print()
        if player == 1 or 2:
            print(entrance.entry(f"Dark corridor {game_data.second_room}"))
            player = input("Yes\nNo\n")
            if player.lower() == "yes":
                self.chest_opened = True
                prize.add_items(bilbo)
                os.system('clear')
                print(bilbo.open_chest("torch"))
                print(prize.description)
            else:
                player.lower() == "no"
                self.chest_opened = False
                pass
            print(game_data.corridor)
            print()
            player = int(input("1. I am not afraid about dark corridors. I go without light.\n2. Back home\n3. Open backpack\n"))
            if player == 2:
                print(game_data.go_home)
                sys.exit()
            elif player == 1:
                print(f'Wow you are brave {entrance.entry("Smaugs cave")[:9].lower() + entrance.entry("Smaugs cave")[9:]}')
            elif player == 3:
                os.system('clear')
                bilbo.show_backpack()
                if self.chest_opened == False:
                    print("Your backpack is empty.")
                else:
                    self.chest_opened == True
                    player = input("Do you want to use torch?\nYes\nNo\n")
                    if player.lower() == "yes":
                        print(game_data.mystery)
                        small_mystery.first_riddle(bilbo)
                    else:
                         player == "No"
                         os.system('clear')
               
    def end_game(self):
        print()
        print(game_data.end_game)
        player = int(input("1. Steal the stown now\n2. Look for another way\n3. Wait for better opportunity\n What you choose?: "))
        if player == 1:
            print()
            print("You have successfully stolen the stone! Congratulations, you have completed the adventure!")
            sys.exit()
        elif player == 2:
            print()
            print("You look around the room and spot a hidden passage. You jump through it and find yourself in another part of the cave. Unfortunately, Smug spotted you and you had to run away, maybe another time it will work.")
            sys.exit()
        else:
            player == 3
            print()
            print("You patiently wait for Smaug to move away. When you think he is far away, you move forward and try to get the stone. Unfortunately, Smaug quickly figures it out and comes back. You have to run away before he can catch you. You managed to escape, but unfortunately you didn't manage to get the stone. You have to go back and find another way to get it.")
            sys.exit()



bilbo = Character("Bilbo Baggins", 2, 100)
smaug = Monsters("Smaug", 10, 50, "flying")
prize = Items("1. torch", "Item Description: Let's you see more")
entrance = Rooms("the chamber of secrets", "every 2 here die")
small_mystery = Mystery("sword_mystery")
menu = Menu("Conquering the Stone of Smaug")
file_path = "/Users/dominikzemirek/Text.game/python-text-game/text.txt"
player = Game("Dominik", 1)
game_data = GameData(file_path)
game_data.read_data()